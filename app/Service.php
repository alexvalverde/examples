<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'services';

    protected $fillable = [
        'category_id',
        'user_id',
        'name',
        'description',
        'price'
    ];

    public function category () {
        $this->belongsTo('\App\Category');
    }

    public function user () {
        $this->belongsTo('\App\User');
    }

    public function tags (){
        return $this->belongsToMany('App\Tag','service_tags', 'service_id', 'tag_id');
    }

}
