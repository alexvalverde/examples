<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Service;
use App\Http\Requests;

class UserServiceController extends Controller
{
    public function index ($id) {
        $user = User::find($id);

        if($user){
            return response()->json('message', 'This User does not exist', 401);
        }

        return response()->json(['data' => $user->services]);
    }

    public function show ($user_id, $id) {

        $user = User::find($user_id);

        if(!$user){
            return response()->json('message', 'This user does not exist', 404);
        }

        $service = $user->services->find($id);

        if(!$service){
            return response()->json('message', 'This service does not exist', 404);
        }

        return response()->json(['data' => $service]);
    }

    public function store (Request $request) {

        $values = $request->only('user_id', 'category_id', 'name', 'description', 'price', 201);

        $service = Service::create($values);

        return response()->json(['data' => $service,  'message' => 'Service correctly added', 201]);
    }

    public function update (Request $request, $user_id, $id) {

        $user = User::find($user_id);

        if(!$user){
            return response()->json('message', 'This user does not exist', 404);
        }

        $service = $user->services->find($id);

        if(!$service){
            return response()->json('message', 'This service does not exist', 404);
        }

        $service->user_id = $request->get('user_id');
        $service->category_id = $request->get('category_id');
        $service->name = $request->get('name');
        $service->description = $request->get('description');
        $service->price = $request->get('price');

        return response()->json(['data' => $service,  'message' => 'Service correctly update'], 201);
    }

    public function destroy ($user_id, $id) {

        $user = User::find($user_id);

        if(!$user){
            return response()->json('message', 'This user does not exist', 404);
        }

        $service = $user->services->find($id);

        if(!$service){
            return response()->json('message', 'This service does not exist', 404);
        }

        $service->delete();

        return response()->json([ 'message' => 'Service has been deleted'], 200);
    }
}
