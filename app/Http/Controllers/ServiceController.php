<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;

use App\Http\Requests;

class ServiceController extends Controller
{
    public function index() {

        $services = Service::all();

        return response()->json(['data' => $services], 200);
    }
}
