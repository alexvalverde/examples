<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profiles';

    protected $fillable = [
        'user_id',
        'first_name',
        'last_name',
        'phone',
        'zip_code',
        'cellphone',
        'picture',
        'address',

    ];
    protected $primaryKey = 'user_id';

    public function user (){
        return $this->belongsTo('App\User');
    }
}
