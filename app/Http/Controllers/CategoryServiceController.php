<?php

namespace App\Http\Controllers;

use App\Category;
use App\Service;
use Illuminate\Http\Request;

use App\Http\Requests;

class CategoryServiceController extends Controller
{
    public function index ($id) {
        $category = Category::find($id);

        if($category){
            return response()->json('message', 'This category does not exist', 401);
        }

        return response()->json(['data' => $category->services]);
    }

    public function show ($category_id, $id) {

        $category = Category::find($category_id);

        if(!$category){
            return response()->json('message', 'This category does not exist', 404);
        }

        $service = $category->service->find($id);

        if(!$service){
            return response()->json('message', 'This service does not exist', 404);
        }

        return response()->json(['data' => $service]);
    }

}
