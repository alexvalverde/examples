<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('users')->delete();

        $users = [
            ['name' => 'admin', 'email' => 'admin@ngc.com', 'password' => Hash::make('admin')]
        ];
        foreach ($users as $user) {
            User::create($user);
        }

        $faker = Faker\Factory::create('es_ES');

        for ($i = 0 ; $i < 20; $i++)
        {
            User::create(
                [
                    'name' => $faker->name,
                    'email' => $faker->unique()->email,
                    'password' => Hash::make('admin')
                ]
            );
        }

        Model::reguard();
    }
}
