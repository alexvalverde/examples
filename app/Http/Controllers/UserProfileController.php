<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Storage;
class UserProfileController extends Controller
{
    public function index ($id){

        $user = User::find($id);
        if(!$user){
            return response()->json(['message' => 'This User does not exist', 'code' => 404], 404);//code not found
        }

        return response()->json(['data' => $user->profile], 200);
    }
    public function store(Request $request)
    {
        $profile = new Profile();
        $profile->user_id = $request->get('user_id');
        $profile->first_name = $request->get('first_name');
        $profile->last_name = $request->get('last_name');
        $profile->zip_code = $request->get('zip_code');
        $profile->address = $request->get('address');
        $profile->cellphone = $request->get('cellphone');
        $profile->phone = $request->get('phone');

        if ($request->hasFile('file') && $request->file('file')->isValid()) {
            $original_name = $request->file('file')->getClientOriginalName();
            $filename = time(). '-' . $original_name;
            $content = file_get_contents($request->file('file')->getRealPath());
            if(Storage::disk('profiles')->put($filename, $content)){
                $profile->picture = $filename;
            }
        }else {
            $profile->picture =  'user_default.png';
        }

        $profile->save();
        return response()->json(['data' => $profile, 'message' => 'Profile correctly added', 'code' => 201], 201); //code Created
    }

    public function show($user_id, $id)
    {
        $user = User::find($user_id);

        if(!$user)
        {
            return response()->json(['message' => 'This User does not exist', 'code' => 404], 404);//code not found
        }

        $profile = $user->profile->find($id);

        if(!$profile)
        {
            return response()->json(['data' => $user, 'message' => 'This Profile does not exist', 'code' => 404], 404);//code not found
        }

        return response()->json(['data' =>  $profile], 200);
    }

    public function update(Request $request, $id, $profile_id)
    {
        $user = User::find($id);

        if(!$user)
        {
            return response()->json(['message' => 'This User does not exist', 'code' => 404], 404);//code not found
        }

        $profile = $user->profile->find($profile_id);

        if(!$profile)
        {
            return response()->json(['message' => 'This Profile does not exist', 'code' => 404], 404);//code not found
        }

        $profile->user_id = $request->get('user_id');
        $profile->first_name = $request->get('first_name');
        $profile->last_name = $request->get('last_name');
        $profile->zip_code = $request->get('zip_code');
        $profile->address = $request->get('address');
        $profile->cellphone = $request->get('cellphone');
        $profile->phone = $request->get('phone');

        if ($request->hasFile('file') && $request->file('file')->isValid()) {
            $original_name = $request->file('file')->getClientOriginalName();
            $exists = Storage::disk('profiles')->exists($original_name);
            if($exists){
                Storage::delete($original_name);
            }
            $filename = time(). '-' . $original_name;
            $content = file_get_contents($request->file('file')->getRealPath());
            if(Storage::disk('profiles')->put($filename, $content)){
                $profile->picture = $filename;
            }
        }else {
            $profile->picture =  'user_default.png';
        }
        $profile->save();

        return response()->json(['data' => $profile, 'message' => 'Profile correctly updated', 'code' => 200], 200);//code Ok
    }

    public function destroy($id, $profile_id)
    {
        $user = User::find($id);

        if(!$user)
        {
            return response()->json(['message' => 'This User does not exist', 'code' => 404], 404);//code not found
        }

        $profile = $user->profile->find($profile_id);

        if(!$profile)
        {
            return response()->json(['message' => 'This Profile does not exist', 'code' => 404], 404);//code not found
        }
        $profile->delete();
        return response()->json(['message' => 'Profile has been deleted', 'code' => 200], 200);
    }
}
