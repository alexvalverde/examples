<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Tag;

class ServiceTagController extends Controller
{
    public function index($tag_id)
    {
        $tag = Tag::find($tag_id);
        if(!$tag)
        {
            return response()->json(['message' => 'This Tag does not exist', 'code' => 404], 404);//code not found
        }
        return response()->json(['data' => $tag->users], 200);
    }

    public function show($service_id, $tag_id)
    {

        $tag = Tag::find($tag_id);
        if(!$tag)
        {
            return response()->json(['message' => 'This Tag does not exist', 'code' => 404], 404);//code not found
        }
        $service = $tag->users->find($service_id);
        if(!$service)
        {
            return response()->json(['message' => 'This Service does not exist', 'code' => 404], 404);//code not found
        }
        return response()->json(['data' => $service], 200);
    }

    public function attachTag(Request $request, $id){
        $tag_id = $request->get('tag_id');
        $service = Service::find($id);
        $service->tags()->attach($tag_id);
        return response()->json($service);
    }

    public function detachTag(Request $request, $id){
        $tag_id = $request->get('tag_id');
        $service = Service::find($id);
        $service->tags()->detach($tag_id);
        return response()->json($service);
    }
}
