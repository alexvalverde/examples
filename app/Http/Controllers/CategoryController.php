<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Category;
use App\Http\Requests;

class CategoryController extends Controller
{
    public function index (){
        $categories = Category::all();
        return response()->json(['data' => $categories], 200);
    }

    public function show ($id){
        $category = Category::find($id);

        if(!$category){
            return response()
                ->json([
                    'message' => 'This category does not  exist'
                ], 404);
        }
        return response()->json(['data' => $category], 200);
    }

    public function store (Request $request){

        $values = $request->only('name');

        $category = Category::create($values);

        return response()->json([
            'data' => $category,
            'message' => 'Category correctly added'
        ], 201);
    }

    public function update (Request $request, $id){
        $category = Category::find($id);

        if(!$category){
            return response()->json([
                'message' => 'This category does not  exist'
            ],404);
        }
        $category->name = $request->get('name');
        $category->save();
        return response()->json([
            'data' => $category,
            'message' => 'Category correctly update'
        ], 201);
    }

    public function destroy ($id){
        $category = Category::find($id);

        if(!$category){
            return response()->json([
                'message' => 'This category does not  exist'
            ], 404);
        }
        $category->delete();
        return response()->json([
                'message' => 'Category has been deleted'
            ],
            200);
    }
}
