<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::group(['prefix' => 'api'],  function ()
{
    Route::resource('users', 'UserController', ['only' => ['index']]);
    Route::resource('services', 'ServiceController', ['only' => ['index']]);
    Route::resource('users.profiles', 'UserProfileController', ['except' => ['create', 'edit']]);
    Route::resource('categories', 'CategoryController', ['except' => ['create', 'edit']]);
    Route::resource('users.services', 'UserServiceController', ['except' => ['create', 'edit']]);
    Route::resource('categories.services', 'CategoryServiceController', ['only' => ['index', 'show']]);

});